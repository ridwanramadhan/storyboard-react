import React from 'react'
import {Header, Content1, ButtonNextPrev} from '../components';

function Question1() {
    return (
        <div className='flex flex-col min-h-screen overflow-x-hidden'>
            <Header title='Recap' desc='Click on the option, then click on the blank to select your answer'/>

            <Content1 question='A voltmeter should be connected in to the electrical component to measure the voltage across it.' ans1='Series' ans2='Parallel'/>

            <ButtonNextPrev back='/' next='/question2'/>
            
        </div>
    )
}

export default Question1
