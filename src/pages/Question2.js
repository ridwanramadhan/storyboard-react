import React from 'react'
import {Header, Content1, ButtonNextPrev} from '../components';

function Question2() {
    return (
        <div className='flex flex-col min-h-screen overflow-x-hidden'>
            <Header title='Circuit 1' desc='Click on two points on the circuit to measure the voltage accross the two points with a volt meter. Fill in the blanks with the voltage measurements obtained for the start parts of the circuit.'/>

            <Content1 question='Ini question 2 bos' ans1='Series' ans2='Parallel'/>

            <ButtonNextPrev back='/question1' next='/'/>

        </div>
    )
}

export default Question2
