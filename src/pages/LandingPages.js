import React from 'react'
import assetGambar from '../assets/img7.svg';

function LandingPages() {
    return (
        <div className='container mx-auto max-h-full'>
            <div className="max-w-7xl mx-auto align-middle py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
                <div className='mt-8 flex flex-col lg:mt-0 lg:flex-shrink-0'>
                    <h1 className='text-6xl font-bold font-sans text-black'>
                        <span className='block'>Voltage in a</span>
                        <span className='block'>series circuit</span>
                    </h1>
                    {/* <h1 className='text-xl font-bold font-sans text-black sm:text-2xl md:text-4xl xl:text-6xl'>
                        <span className='block'>Voltage in a</span>
                        <span className='block'>series circuit</span>
                    </h1> */}
                    <a
                        href="/question1"
                        className="mt-12 w-20 text-center bg-gray-200 border border-gray-500 rounded-md py-3 font-medium text-gray-900 hover:bg-gray-300"
                    >
                        Start
                    </a>
                </div>
                <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
                    <div className="w-96 h-96 inline-flex">
                        <img src={assetGambar} alt='assetGambar'></img>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LandingPages
