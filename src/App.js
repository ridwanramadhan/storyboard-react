import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LandingPages from './pages/LandingPages.js';
import Question1 from './pages/Question1.js'
import Question2 from './pages/Question2.js'

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<LandingPages />}/>
        <Route path="/question1" element={<Question1 />}/>
        <Route path="/question2" element={<Question2 />}/>
      </Routes>
    </Router>
  );
}

export default App;
