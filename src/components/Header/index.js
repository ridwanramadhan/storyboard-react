import React from 'react'

const Header = ({title, desc}) => {
    return (
        <div className='flex flex-col sm:flex-row mx-10 mt-10 border-gray-200 bg-gray-200 rounded-lg shadow-lg'>
            <div className='sm:w-1/6 shadow-sm p-6 space-y-6 border-solid border-4 border-purple-500 bg-purple-500 rounded-lg w-17 font-bold text-2xl lg:text-4xl text-white font-sans text-center'>{title}</div>
            <div className='flex w-full h-auto p-6 font-light text-xl font-sans items-center'>
                {desc}
            </div>
        </div>
    )
}

export default Header
