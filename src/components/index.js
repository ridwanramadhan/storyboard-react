import Header from './Header';
import Content1 from './Content1';
import ButtonNextPrev from './ButtonNextPrev';

export {Header, Content1, ButtonNextPrev};