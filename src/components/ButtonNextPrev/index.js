import React from 'react'
import {Link} from 'react-router-dom'

const ButtonNextPrev = ({back, next}) => {
    return (
        <div className='flex justify-between mt-16 md:mt-72 lg:mt-80 mx-10'>
            <Link to={back} className='bg-purple-500 hover:bg-purple-700 text-white text-3xl w-12 h-10 px-3 rounded-l-full'>&#8592;</Link>
            <Link to={next} className='bg-purple-500 hover:bg-purple-700 text-white text-3xl w-12 h-10 px-3 rounded-r-full'>&#8594;</Link>
        </div>
    )
}

export default ButtonNextPrev
