import React from 'react'

const Content1 = ({question, ans1, ans2}) => {
    return (
        <div>
            <h3 className='question text-3xl text-center mt-14 lg:mt-40 md:mt-36 mx-10 font-sans'> 
                {question}
            </h3>
            <div className='flex justify-center'>
                <button class="bg-purple-500 hover:bg-purple-700 text-white font-bold text-xl py-3 px-5 items-center mt-10 rounded-2xl shadow-lg">
                    {ans1}
                </button>
            </div>
            <div className='flex justify-center'>
                <button class="bg-purple-500 hover:bg-purple-700 text-white font-bold text-xl py-3 px-5 items-center mt-10 rounded-2xl shadow-lg">
                    {ans2}
                </button>
            </div>
        </div>
    )
}

export default Content1
